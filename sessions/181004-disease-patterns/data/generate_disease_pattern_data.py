import pandas as pd
import numpy as np

# Set the RNG seed
np.random.seed(0)

# Number of observations
n = 1000

# Simulate data from a three-tier DAG

# Tier 0
d1 = np.random.binomial(1, 0.6, n)
# Tier 1
d2 = np.random.binomial(1, 0.3 * d1 + 0.2)
d3 = np.random.binomial(1, 0.6 * d1 + 0.1)
# Tier 2
d4 = np.random.binomial(1, 0.2 * d2 + 0.3)
d5 = np.random.binomial(1, 0.4 * d2 + 0.5 * d3 + 0.1)
# Tier 3
d6 = np.random.binomial(1, 0.5 * d4 + 0.5 * d5 + 0)

# Combine in dataframe
data = pd.DataFrame(dict(
    d1=d1,
    d2=d2,
    d3=d3,
    d4=d4,
    d5=d5,
    d6=d6))

# Rename the index to id
data.index = data.index.rename('id')

# Export to CSV
data.to_csv('disease-patterns.csv')
