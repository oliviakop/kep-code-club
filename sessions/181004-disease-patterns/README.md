# 181004 - Disease Patterns

For this session we are interested in the problem of finding the frequencies
with which certain variables occur together, e.g. diseases or co-morbidities.

## Example data
Our example data describe patients with an ID and binary indicators for having
disease 1 through 6. A CSV file with these data can be found in the data folder.
The table below shows the first 10 observation.

|  id |  d1 |  d2 |  d3 |  d4 |  d5 |  d6 |
| --- | --- | --- | --- | --- | --- | --- |
|   0 |   1 |   1 |   0 |   0 |   0 |   0 |
|   1 |   0 |   0 |   0 |   0 |   0 |   0 |
|   2 |   0 |   0 |   0 |   1 |   0 |   0 |
|   3 |   1 |   1 |   1 |   1 |   1 |   1 |
|   4 |   1 |   0 |   1 |   1 |   1 |   1 |
|   5 |   0 |   1 |   0 |   0 |   0 |   0 |
|   6 |   1 |   1 |   1 |   1 |   1 |   1 |
|   7 |   0 |   0 |   0 |   0 |   0 |   0 |
|   8 |   0 |   0 |   0 |   0 |   0 |   0 |
|   9 |   1 |   1 |   1 |   1 |   1 |   1 |

**Now we have 3 questions we want to find the answer to:**

1. What is the frequency of each unique disease pattern?
2. For all combinations of two diseases, what are their frequencies?
3. For all possible combinations of diseases, what are their frequencies?

*Hint: For all problems, you do not have to care about the order of the
variables, meaning that having d2 + d3 is equivalent to having d3 + d2.*

## 1. Unique patterns
What is the frequency of each unique disease pattern? Given the first 10
observations above, we are looking for something like this:

|   Patterns   | freq |
| ------------ | ---- |
| No disease   |    3 |
| All diseases |    3 |
| Only d2      |    1 |
| Only d4      |    1 |
| d1 and d2    |    1 |
| All but d2   |    1 |

*Hint: Try combining all the variables into one and count the number of unique
patterns in that variable.*

## 2. Frequencies of two diseases
What are the frequencies for all combinations of two diseases, independent of
any other diseases? Given the first 10 observations above, we are looking for
something like this:

| Combination | freq |
| ----------- | ---- |
| d5 + d6     |    4 |
| d4 + d6     |    4 |
| d4 + d5     |    4 |
| d3 + d6     |    4 |
| d3 + d5     |    4 |
| d3 + d4     |    4 |
| d1 + d6     |    4 |
| d1 + d5     |    4 |
| d1 + d4     |    4 |
| d1 + d3     |    4 |
| d1 + d2     |    4 |
| d2 + d6     |    3 |
| d2 + d5     |    3 |
| d2 + d4     |    3 |
| d2 + d3     |    3 |

*Hint: It might be useful to look into the concept of
[combinations](https://en.wikipedia.org/wiki/Combination).*

## 3. Frequencies of any number of diseases
What are the frequencies for all possible combinations of diseases, independent
of any other diseases? This is a generalization of question 2. Given the first
10 observations above, we are looking for something like this:

| Combination  | freq |
| ------------ | ---- |
| d1           | 5    |
| d2           | 5    |
| d4           | 5    |
| d3 + d5      | 4    |
| d1 + d4 + d6 | 4    |
| d1 + d4 + d5 | 4    |
| d1 + d3 + d6 | 4    |
| d1 + d3 + d5 | 4    |
| d1 + d3 + d4 | 4    |
| d5 + d6      | 4    |
| d4 + d6      | 4    |
| d4 + d5      | 4    |
| ...          | ...  |

*Hint: 64 possible combinations.*
