import pandas as pd # type: ignore

###############################################################################
# Read the data
###############################################################################
# ICD data
codes = pd.read_csv('../data/icd/codes.csv')
categories = pd.read_csv('../data/icd/categories.csv')
subchapters = pd.read_csv('../data/icd/subchapters.csv')
chapters = pd.read_csv('../data/icd/chapters.csv')

# Exercise data
raw_data = pd.read_csv(
    '../data/visits.csv',
    parse_dates=['visit_date', 'onset_date'])

# Filter invalid codes
has_valid_code = raw_data['main_dia'].isin(codes['code'])

# Invalid data
invalid_data = raw_data[~has_valid_code].copy()

# Valid data
data = raw_data[has_valid_code].copy()

###############################################################################
# Solution
###############################################################################
def icd_category(codes: pd.Series, categories: pd.Series) -> pd.Series:
    """ Categorize icd codes into subchapters or chapters """
    # Get the category from the code and convert to int with base 36
    values = codes.str[0:3].apply(int, base=36)

    # Pick the end of the interval and convert to int with base 36
    # Need to add 0 as the start of the interval
    cat = [0] + categories.str[4:].apply(int, base=36).tolist()

    # Return the categorized data with the correct labels
    return pd.cut(values, cat, labels=categories)


# Categorize data
data['code'] = data['main_dia']
data['category'] = data['code'].str[:3]
data['subchapter'] = icd_category(data['code'], subchapters['code'])
data['chapter'] = icd_category(data['code'], chapters['code'])

# Check if visit_date is before 2014
data['before_2014'] = data['visit_date'].dt.year < 2014

# Check if visit_date is before onset_date
data['before_onset'] = data.eval('visit_date < onset_date')

###############################################################################
# 1. How many visits have a main diagnosis from the category "M05"?
###############################################################################
print('Visits with M05 category:',
    data['category'].value_counts()['M05'])

# print('Visits with M05 codes:',
#     data.query('category == "M05"')['code'].value_counts(), sep='\n')

###############################################################################
# 2 How many visits have a main diagnosis from chapter 10 (J00–J99)?
###############################################################################
print('Visits with J00-J99 chapter:',
    data['chapter'].value_counts()['J00-J99'])

# print('Visits with J00-J99 codes:',
#     data.query('chapter == "J00-J99"')['code'].value_counts(), sep='\n')

###############################################################################
# 3. How many patients have at least one main diagnosis from chapter 10 (J00–J99)?
###############################################################################
print('Patients with J00-J99 chapter:',
    data.query('chapter == "J00-J99"')['patient_id'].nunique())

###############################################################################
# 4. Within visits, which subchapters have a frequency above 1%?
###############################################################################
vis_subch = data['subchapter'].value_counts(normalize=True)

print('Visits, subchapters with freq above 1%:',
    vis_subch[vis_subch >= 0.01], sep='\n')

###############################################################################
# 5. Within patients, which subchapters have a frequency above 1%?
###############################################################################
pat_subch = data[['patient_id', 'subchapter']] \
    .drop_duplicates()['subchapter'].value_counts(normalize=True)

print('Patients, subchapters with freq above 1%:',
    pat_subch[pat_subch >= 0.01], sep='\n')

###############################################################################
# 6. How many patients have a code for "Multiple Sclerosis"?
###############################################################################
print('Patients with MS diagnosis:',
    data.query('category == "G35"')['patient_id'].nunique())

###############################################################################
# 7. Are there any invalid codes in the dataset? If so, how many?
###############################################################################
print('Number of invalid diagnoses:',
    invalid_data.shape[0])

print('Number of invalid diagnoses with correct format:',
    invalid_data['main_dia'].str.match('^[A-Z][0-9]{3}[A-Z0-9]?$').sum())

print('Invalid diagnoses with correct format:',
    invalid_data.loc[
        invalid_data['main_dia'].str.match('^[A-Z][0-9]{3}[A-Z0-9]?$', na=False),
        'main_dia'], sep='\n')
