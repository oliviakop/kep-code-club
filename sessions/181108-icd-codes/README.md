# 181108 - ICD Codes

Most of us in epidemiology work with ICD codes in one way or another. ICD stands
for International Classification of Diseases and has been published by the World
Health Organization (WHO) since 1992.

The 10th revision (ICD10) is currently in use in Sweden where Socialstyrelsen is
responsible for the translation. It is sometimes also referred to as KSH97
(Klassifikation av Sjukdomar och Hälsoproblem 1997). In Sweden, the ICD is used
in the clinic, for reimbursement by hospitals, and in our national healthcare
registers.

ICD is structured into 22 chapters, that are further divided into sub-chapters,
categories, and codes.

More information about the ICD in Sweden is available on Socialstyrelsen's
website at
https://www.socialstyrelsen.se/klassificeringochkoder/diagnoskodericd-10
(Swedish), with files for download at
https://www.socialstyrelsen.se/klassificeringochkoder/laddaner/kodtextfiler

## Exercise
In this exercise, you get a dataset with (mock) visits to a hospital. Each row
consists of a patient ID, a date for the visit, an onset date, and some
diagnosis codes. Your assignment is to categorize the main diagnosis code into
the correct category, sub-chapter, and chapter, respectively, and then use this
data to answer the following questions:

1. How many visits have a main diagnosis from the category "M05"?
2. How many visits have a main diagnosis from chapter 10 (J00–J99)?
3. How many patients have at least one main diagnosis from chapter 10 (J00–J99)?
4. Within visits, which sub-chapters have a frequency of 1% or more?
5. Within patients, which sub-chapters have a frequency of 1% or more?
6. How many patients have a code for "Multiple Sclerosis"?
7. Are there any invalid codes in the dataset? If so, how many?

## Exercise data
An exercise dataset can be found in the data folder.

Table 1: First five rows of exercise dataset

| patient_id | visit_date | onset_date | main_dia |      diagnoses      |
| ---------- | ---------- | ---------- | -------- | ------------------- |
|        173 | 2011-12-03 | 2014-07-29 | I312     | I312                |
|         48 | 2011-06-15 | 2013-02-28 | T815     | T815 H111 P917 A001 |
|        118 | 2013-01-04 | 2016-06-24 | P522     | P522 M058L          |
|        193 | 2012-01-09 | 2013-02-01 | M058X    | M058X N005 S0261    |
|        252 | 2015-02-27 | 2016-02-27 | D849     | D849 Q898F          |

## Example stuctured data (first 5 rows)

Table 2: First five rows of exercise dataset with ICD category, subchapter, and
chapter.

| patient_id |  code | category | subchapter | chapter |
| ---------- | ----- | -------- | ---------- | ------- |
|        173 | I312  | I31      | I30-I52    | I00-I99 |
|         48 | T815  | T81      | T80-T88    | S00-T98 |
|        118 | P522  | P52      | P50-P61    | P00-P96 |
|        193 | M058X | M05      | M05-M14    | M00-M99 |
|        252 | D849  | D84      | D80-D89    | D50-D89 |

## ICD data
Files for the ICD codes, categories, subchapters, and chapters can be found in
the data/icd sub-folder.
