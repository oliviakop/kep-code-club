import requests, io
import numpy as np
import pandas as pd
from zipfile import ZipFile

# USA codes: https://www.cms.gov/Medicare/Coding/ICD10/Downloads/2019-ICD-10-CM-Code-Descriptions.zip

###############################################################################
# Get ICD codes
###############################################################################
def make_dataset(file):
    """ Processes the file contents and creates a dataframe """
    # Decode the file to text
    file_contents = file.read().decode("cp1252")
    # Separate code and description
    data = [[t[:7].strip(), t[7:]] for t in file_contents.split('\r\n') if t]
    # Create DataFrame
    df = pd.DataFrame(data, columns=['code', 'desc'])
    # Remove U codes
    df = df[~df['code'].str.startswith('U')]
    # Return the DataFrame
    return df

# URL to zip archive with ICD codes
icd_url = 'https://www.socialstyrelsen.se/SiteCollectionDocuments/ANSI-2018.zip'

# Files to extract and process
files = dict(
    chapters='KSH97_KAP.ANS',
    subchapters='KSH97_AVS.ANS',
    categories='KSH97_KAT.ANS',
    codes='KSH97_KOD_20171218.txt')

# Download file
r = requests.get(icd_url)

# Result dictionary
icd = dict()

# If download ok, extract files from list and make dataframes
if r.ok:
    with ZipFile(io.BytesIO(r.content)) as z:
        for name, file in files.items():
            with z.open(file) as f:
                icd[name] = make_dataset(f)

# Export the cleaned ICD data
for name, ds in icd.items():
    ds.to_csv(f'icd/{name}.csv', index=False)

###############################################################################
# Create data
###############################################################################
n = 1000
patient_n = 300
max_dia = 5
min_date = '2011-01-01'
max_date = '2016-12-31'
error_prop = 0.1
likely_cats = ['M05', 'G35']

# Calculate date difference for later use
date_diff_d = (pd.to_datetime(max_date) - pd.to_datetime(min_date)).days

# Set the seed for reproducibility
rng = np.random.RandomState(0)

# Patient ID
patient_id = rng.randint(1, patient_n + 1, size=n)

# Date of visit
visit_date = pd.to_datetime(min_date) + \
    pd.to_timedelta(rng.randint(date_diff_d + 1, size=n), 'd')

# Create DataFrame
data = pd.DataFrame(dict(
    patient_id=patient_id,
    visit_date=visit_date))

###############################################################################
# Date of onset
###############################################################################
# Date of onset, needs to be the same in the same patient
onset_date = pd.Series(
    pd.to_datetime(min_date) + pd.to_timedelta(rng.randint(
        date_diff_d + 1, size=data['patient_id'].nunique()), 'd'),
    index=data['patient_id'].unique(), name='onset_date')
# Add onset_date to data
data = data.join(onset_date, on='patient_id')

###############################################################################
# Diagnoses
###############################################################################
# Assign all codes an equal probability
probs = icd['codes'].assign(p=1)

# Boost probability of all codes in the likely categories to 1 % per code
likely_codes = probs['code'].str[:3].isin(likely_cats)
probs.loc[likely_codes, 'p'] = \
    0.01 * (probs.shape[0] - likely_codes.sum()) / (1 - 0.01 * likely_codes.sum())

# Convert to actual probabilities
p = probs['p'] / probs['p'].sum()

# Diagnoses
diagnoses = pd.DataFrame(
    rng.choice(icd['codes']['code'], size=(n, max_dia), p=p).tolist())

###############################################################################
# Errors
###############################################################################
# Generate random error locations
error_loc = pd.DataFrame(rng.binomial(1, error_prop, size=(n, max_dia))).astype(bool)

# Generate invalid diagnosis codes
error_df = pd.DataFrame(
    np.vectorize(lambda x: np.base_repr(x, base=36)[:rng.randint(10)])(
        rng.randint(int('ZZZZZ', base=36),
            size=(n, max_dia), dtype=np.int64)))

# Add invalid diagnoses to data at error locations
diagnoses = diagnoses.mask(error_loc, error_df, axis=1)

# Add a custom error in the right format but not from list
diagnoses.loc[rng.randint(n), 0] = 'D490'

###############################################################################
# Add diagnoses to data
###############################################################################
# Pick the first diagnosis as main diagnosis
data['main_dia'] = diagnoses[0]

# Keep only some diagnoses and convert to list, make sure only unique codes
data['diagnoses'] = diagnoses.apply(
    lambda x: ' '.join(x.unique()[:rng.randint(1, x.nunique())]), axis='columns')

# Reorder columns
data = data[['patient_id', 'visit_date', 'onset_date', 'main_dia', 'diagnoses']]

# Export to csv
data.to_csv('visits.csv', index=False)
