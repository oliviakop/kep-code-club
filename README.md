# Welcome to the KEP CODE CLUB!

Struggling with your code? Don’t know how to make SAS/R/Python/whatever program
you’re using prepare data the way you want it to? Fear not! This semester we
are starting the KEP code club, a place where we will discuss real world
programming problems. Everyone, regardless of previous programming experience,
is welcome to join!

## What this is and how it will work
This is a place to discuss and share your struggles and successes related to
programming and data preparation. We will each meeting discuss a programming
problem or concept. Regardless of what software or programming language you are
using and what experience you may or may not have, you are welcome to join! This
club is just as much about learning the process of how to solve programming
problems, as learning the best solution for a specific problem (hint! There may
be more than one good solution!). To achieve this, we will focus on discussions
of problems. For every meeting, we will send out a problem before hand, and the
minimum requirement for participation is to have read the problem. If you feel
like you can, please try to prepare a solution. If you feel like it’s too hard
and you don’t even know where to start, please come and listen to the discussion
and join in as much as you can, it’s a great way to learn! If you have a
specific problem you’re struggling with, you are welcome to send it to the
organizers before so we can prepare and send it out to the participants. You are
also most welcome to send us problems that you did solve, but want to discuss
other ways of solving them. There will each meeting also be set aside some time
for questions and discussions of smaller problems the participants might have.

## What this is not
This is not the place to discuss statistics and (epi) methodology or problems
with specific data sets, data sources, or variables. There are other forums at
KEP better suited for that.

**Hope to see you there!**

Helga, Computer scientist  
Kelsi, SAS newbie  
Peter, Python enthusiast
